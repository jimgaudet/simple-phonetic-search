# Simple Phonetic Search

## How to use

First save to your project.

```javascript
npm install -S simple-phonetic-search@latest
yarn add simple-phonetic-search@latest
```

Then run the following code below in your project.

```javascript
const SimplePhoneticSearch = require('simple-phonetic-search').default;
const search = new SimplePhoneticSearch();

const result1 = search.StringsSoundAlike('Jim', 'Gym');
const result2 = search.StringsSoundAlike('Jim', 'Jeff');

console.info("Testing Jim and Gym", result1);
console.info("Testing Jim and Jeff", result2);
```

You will get a result object that looks like this.

```javascript
let result = {

    success: true,
    message: ''
};
```

## Versions

Version 1.0.0

- Beginning

Version 1.0.1

- Use of 'prepublish' deprecated ([Screenshot of Publish warning](https://www.screencast.com/t/9BzyrWzdDkz)) converting to prepare
- Added README.md

Version 1.0.2

- Updated README.md with 'how to'
- NOTE: README.md will be updated with each version so no need to add it anymore

Version 1.0.3

- Fixed bug where some names were returning similar when they were not. I.e. "Jim" and "Gaudet"

Version 1.0.4

- Forgot to remove some console.info() lines... :(
