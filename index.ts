const DoubleMetaphone: any = require('double-metaphone');

export interface ISimplePhoneticSearchResult {

    success: boolean;
    message: any;
}

export interface ISimplePhoneticSearch {

    StringsSoundAlike(s1: string, s2: string): ISimplePhoneticSearchResult;
}

export class SimplePhoneticSearch implements ISimplePhoneticSearch {

    private GetMetaphoneKeys(s: string): string[] {
     
        const $_: SimplePhoneticSearch = this;

        let valid_keys: string[] = [];

        try {
            
            const keys: string[] = DoubleMetaphone(s);
            const set: Set<string> = new Set();

            keys.forEach(key => set.add(key));
            set.forEach(item => valid_keys.push(item));
        } catch (error) {
            
            console.error('SimplePhoneticSearch -> GetMetaphoneKeys -> ', s, error);
        }

        return valid_keys;
    }

    public StringsSoundAlike(s1: string, s2: string): ISimplePhoneticSearchResult {

        const $_: SimplePhoneticSearch = this;
        const result: ISimplePhoneticSearchResult = {

            success: false,
            message: ''
        };

        try {

            const one: string[] = $_.GetMetaphoneKeys(s1);
            const two: string[] = $_.GetMetaphoneKeys(s2);
            const valid_keys: number = one.length + two.length;
            
            const set: Set<string> = new Set();

            one.forEach(x => set.add(x));
            two.forEach(y => set.add(y));

            result.success = set.size < valid_keys;
            result.message = set.size < valid_keys ? '' : 'Sorry, these strings do not sound alike.';
        } catch (error) {

            result.success = false;
            result.message = {

                Class: '',
                Function: '',
                String_1: s1,
                String_2: s2,
                Error: error
            };

            console.error(result);
        }

        return result;
    }
}

export default SimplePhoneticSearch;
