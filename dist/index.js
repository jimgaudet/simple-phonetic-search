"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DoubleMetaphone = require('double-metaphone');
class SimplePhoneticSearch {
    GetMetaphoneKeys(s) {
        const $_ = this;
        let valid_keys = [];
        try {
            const keys = DoubleMetaphone(s);
            const set = new Set();
            keys.forEach(key => set.add(key));
            set.forEach(item => valid_keys.push(item));
        }
        catch (error) {
            console.error('SimplePhoneticSearch -> GetMetaphoneKeys -> ', s, error);
        }
        return valid_keys;
    }
    StringsSoundAlike(s1, s2) {
        const $_ = this;
        const result = {
            success: false,
            message: ''
        };
        try {
            const one = $_.GetMetaphoneKeys(s1);
            const two = $_.GetMetaphoneKeys(s2);
            const valid_keys = one.length + two.length;
            const set = new Set();
            one.forEach(x => set.add(x));
            two.forEach(y => set.add(y));
            result.success = set.size < valid_keys;
            result.message = set.size < valid_keys ? '' : 'Sorry, these strings do not sound alike.';
        }
        catch (error) {
            result.success = false;
            result.message = {
                Class: '',
                Function: '',
                String_1: s1,
                String_2: s2,
                Error: error
            };
            console.error(result);
        }
        return result;
    }
}
exports.SimplePhoneticSearch = SimplePhoneticSearch;
exports.default = SimplePhoneticSearch;
