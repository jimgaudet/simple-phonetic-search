export interface ISimplePhoneticSearchResult {
    success: boolean;
    message: any;
}
export interface ISimplePhoneticSearch {
    StringsSoundAlike(s1: string, s2: string): ISimplePhoneticSearchResult;
}
export declare class SimplePhoneticSearch implements ISimplePhoneticSearch {
    private GetMetaphoneKeys(s);
    StringsSoundAlike(s1: string, s2: string): ISimplePhoneticSearchResult;
}
export default SimplePhoneticSearch;
