const SimplePhoneticSearch = require('./dist/index').default;
const search = new SimplePhoneticSearch();

const result1 = search.StringsSoundAlike('Jim', 'Gym');
const result2 = search.StringsSoundAlike('Jim', 'Jeff');
const result3 = search.StringsSoundAlike('Jim', 'Gaudet');

console.info("Testing Jim and Gym", result1);
console.info("Testing Jim and Jeff", result2);
console.info("Testing Hell and Gaudet", result3);